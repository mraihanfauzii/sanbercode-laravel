<?php

class Ape extends Animal {
    public function yell() {
        return "Auooo";
    }

    // Override property
    public $legs = 2;
}
?>