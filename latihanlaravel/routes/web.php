<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// CRUD Cast
// Create
// Route yang mengarah ke halaman tambah cast
Route::get('/cast/create', [CastController::class, 'create']);
// Route untuk menyimpan data inputan masuk ke database & validasi
Route::post('/cast', [CastController::class, 'store']);

// Read
// Route yang menampilkan semua data di database ke blade web browser
Route::get('/cast', [CastController::class, 'index']);
// Route detail data berdasarkan id
Route::get('/cast/{id}', [CastController::class, 'show']);

// Update
//Route untuk mengarah ke halaman form update data berdasarkan id
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
//Route untuk update data ke database berdasarkan id
Route::put('/cast/{id}', [CastController::class, 'update']);

// Delete
// Route untuk mendelete berdasarkan id
Route::delete('/cast/{id}', [CastController::class, 'destroy']);