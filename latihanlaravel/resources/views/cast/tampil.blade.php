
<h1>Tampil Cast</h1>

<a href="/cast/create">Tambah Cast</a>

<table class="table">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $item)
        <tr>
        <th scope="row">{{ $key + 1 }}</th>
        <td>{{ $item->nama }}</td>
        <td>{{ $item->umur }}</td>
        <td>{{ $item->bio }}</td>
        <td>
          <form action="/cast/{{$item->id}}" method="post">
            @csrf
            @method('delete')
            <a href="/cast/{{$item->id}}">Detail</a>
            <a href="/cast/{{$item->id}}/edit">Edit</a>
            <input type="submit" value="Delete">
          </form>
        </td>
        </tr>
    @empty
      <tr>
        <td> Tidak Ada Cast </td>
      </tr>
    @endforelse
  </tbody>
</table>